#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Sign values
#define POS_SIGN 1
#define NIL_SIGN 0
#define NEG_SIGN -1

// Error codes
#define PARSE_ERROR -666

// Helper functions
#define GET_SIZE(bi) (bi->size)
#define GET_SIGN(bi) (bi->sign)
#define GET_DATA(bi) (bi->data)
#define SET_SIZE(bi, sz) (bi->size = sz)
#define SET_SIGN(bi, si) (bi->sign = si)
#define SET_DATA(bi, d) (bi->data = d)
#define INT_TO_CHAR(i) (i + 48)
#define CHAR_TO_INT(c) (c - 48)
#define IS_NUMERIC(c) (c >= 48 && c < 58)

struct BigInt {
    int size, sign;
    char *data;
};

typedef struct BigInt BigInt;

/**
 * Validates a string is formatted as number.
 * Takes strings as variadic parameter.
 * Examples:
 * "123", "+123", "-123"        -> 1
 * "s123", "hello", "133T"      -> Parse error
 * */
int validate_v(int n, ...) {
    va_list args;
    va_start(args, n);
    while (n-- > 0) {
        char *s = va_arg(args, char *);
        if (s[0] != '+' && s[0] != '-' && !IS_NUMERIC(s[0])) {
            printf("Cannot parse \\”%s\"\n", s);
            return PARSE_ERROR;
        }
        for (int i = 1; i < strlen(s); i++)
            if (!IS_NUMERIC(s[i])) {
                printf("Cannot parse \\”%s\"\n", s);
                return PARSE_ERROR;
            }
    }
    return 1;
}

/**
 * Accepts a string formatted as a number.
 * Returns pointer to a BigInt.
 * */
BigInt *parse(char *s) {
    assert(validate_v(1, s) == 1);
    BigInt *bi;
    int n;
    bi = malloc(sizeof(BigInt));
    n = strlen(s);
    switch (s[0]) {
        case '+':
        case '-':
            SET_SIZE(bi, n - 1);
            SET_SIGN(bi, s[0] == '+' ? POS_SIGN : NEG_SIGN);
            SET_DATA(bi, &s[1]);
            break;
        default:
            SET_SIZE(bi, n);
            SET_SIGN(bi, POS_SIGN);
            SET_DATA(bi, s);
            break;
    }
    return bi;
}

/**
 * Greater than.
 * Is b1 > b2?
 * */
int gt(BigInt *b1, BigInt *b2) {
    if (GET_SIZE(b1) > GET_SIZE(b2) || GET_SIGN(b1) > GET_SIGN(b2))
        return 1;
    else if (GET_SIZE(b2) > GET_SIZE(b1) || GET_SIGN(b2) > GET_SIGN(b1))
        return 0;
    else {
        // b1 and b2 are of equal length here
        for (int i = GET_SIZE(b1) - 1; i >= 0; i--) {
            if (GET_SIGN(b1)) {
                if (GET_DATA(b1)[i] > GET_DATA(b2)[i]) {
                    return 1;
                }
            } else {
                if (GET_DATA(b2)[i] > GET_DATA(b1)[i]) {
                    return 1;
                }
            }
        }
    }
    return 0;
}

/**
 * Less than.
 * Is b1 < b2?
 * */
int lt(BigInt *b1, BigInt *b2) { return gt(b2, b1); }

/**
 * Equality.
 * If signs and data are identical, we have equality.
 * */
int eq(BigInt *b1, BigInt *b2) {
    return GET_SIGN(b1) == GET_SIGN(b2) && !strcmp(GET_DATA(b1), GET_DATA(b2));
}

/**
 * Get max size in a collection of BigInts.
 * BigInts given as variadic parameter.
 * */
int max_size_v(int n, ...) {
    va_list args;
    va_start(args, n);

    int r = 0;
    for (int i = 0; i < n; i++) {
        BigInt *b = va_arg(args, BigInt *);
        if (GET_SIZE(b) > r) r = GET_SIZE(b);
    }
    return r;
}

/**
 * Get max size in a collection of BigInts.
 * BigInts given as an array.
 * */
int max_size_arr(int n, BigInt *args[]) {
    int r = 0;
    for (int i = 0; i < n; i++) {
        BigInt *b = args[i];
        if (GET_SIZE(b) > r) r = GET_SIZE(b);
    }
    return r;
}

BigInt *add(BigInt *b1, BigInt *b2) {
    BigInt *res;
    char *dat;
    int a, b, i, j, k, sum, new_sz;
    res = malloc(sizeof(BigInt));
    new_sz = max_size_v(2, b1, b2) + 1;
    dat = malloc(1 + new_sz * sizeof(char));
    sum = dat[new_sz] = 0;
    SET_SIGN(res, POS_SIGN);
    SET_SIZE(res, new_sz);
    i = GET_SIZE(b1) - 1;
    j = GET_SIZE(b2) - 1;
    k = new_sz - 1;
    while (k >= 0) {
        a = (i >= 0) ? CHAR_TO_INT(GET_DATA(b1)[i--]) : 0;
        b = (j >= 0) ? CHAR_TO_INT(GET_DATA(b2)[j--]) : 0;
        sum = a + b + (sum / 10);
        dat[k--] = INT_TO_CHAR(sum % 10);
    }
    dat[0] == 48 ? SET_DATA(res, &dat[1]) : SET_DATA(res, dat);
    return res;
}

/*
res = size(b1) + size(b2) = 4

k = 0
5 * 112 = 560
b1[0] * b2[0]
= 5 * 1 = 5
res[k] = 5          

res = 5000

k = 1
b1[0] * b2[1]
= 5 * 1 = 5
res[k] = 5

res = 5500

k = 2
b1[0] * b2[2]
= 5 * 2 = 10
res[k] = mod10 = 0
res[k-1] += div10 = 5 + 1 = 6

res = 5600

done, trim last zero off


*/
BigInt *mult(BigInt *b1, BigInt *b2) {
    BigInt *res;
    char *dat;
    int a, b, i, j, k, sum, new_sz;
    res = malloc(sizeof(BigInt));
    new_sz = GET_SIZE(b1) + GET_SIZE(b2);
    dat = malloc(1 + new_sz * sizeof(char));
    dat[new_sz] = 0;
    sum = 1;
    SET_SIGN(res, POS_SIGN);
    SET_SIZE(res, new_sz);
    i = 0;
    j = 0;
    k = 0;
    while (k < new_sz) {
        a = (i < GET_SIZE(b1)) ? CHAR_TO_INT(GET_DATA(b1)[i++]) : 1;
        b = (j < GET_SIZE(b2)) ? CHAR_TO_INT(GET_DATA(b2)[j++]) : 1;
        if(!(sum / 10)) sum = 10;
        sum = a * b * (sum / 10);
        dat[k++] = INT_TO_CHAR(sum % 10);
        printf("i %d, j %d, k %d, a %d, b %d, sum %d\n", i-1, j-1, k-1, a, b, sum);
    }
    dat[0] == 48 ? SET_DATA(res, &dat[1]) : SET_DATA(res, dat);
    return res;
}

/**
 * Takes strings a variadic parameter.
 * Converts them to BigInts and prints stats.
 * */
void debug_v(int n, ...) {
    va_list args;
    va_start(args, n);

    printf("Sign\tSize\tData\n");
    for (int i = 0; i < n; i++) {
        char *s = va_arg(args, char *);
        BigInt *bi = parse(s);
        printf("%d\t%d\t%s\n", GET_SIGN(bi), GET_SIZE(bi), GET_DATA(bi));
    }
}

/**
 * Takes strings as array.
 * Converts them to BigInts and prints stats.
 * */
void debug_arr(int n, char *args[]) {
    printf("Sign\tSize\tData\n");
    for (int i = 0; i < n; i++) {
        BigInt *bi = parse(args[i]);
        printf("%d\t%d\t%s\n", GET_SIGN(bi), GET_SIZE(bi), GET_DATA(bi));
    }
}

int main(int argc, char *argv[]) {
    printf("Sum is: %s\n", GET_DATA(mult(parse(argv[1]), parse(argv[2]))));
    return 0;
}