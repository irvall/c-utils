
/* 
* General idea: StrShift class contains a single string object. 
* Upon shifting we create two substrings that have been shifted accordingly.
* Modulo is used to allow shifting n times when n > [string this->length].
* Usage: 
*     StrShift (no args) 
*     StrShift <string> <shift count>
*/
#include <iostream>
#include <string>
using namespace std;

class StrShift
{
      // Functionality for pretty printing our class
      friend ostream &operator<<(ostream &os, const StrShift &sshift);

public:
      StrShift(const string s)
      {
            this->stringContent = s;
      }
    
      StrShift() 
      {
            this->stringContent = "";
      }

      // overloading the lshift (<<) operator
      string operator<<(const int n)
      {
            int length = this->stringContent.length();
            if (length == 0)
                  return this->stringContent;
            int shift = (length - n % length) % length;
            this->stringContent = this->stringContent.substr(shift, length) + this->stringContent.substr(0, shift);
            return this->stringContent;
      }

      // overloading the rshift (>>) operator
      string operator>>(const int n)
      {
            int length = this->stringContent.length();
            if (length == 0)
                  return this->stringContent;
            int shift = n % length;
            this->stringContent = this->stringContent.substr(shift, length) + this->stringContent.substr(0, shift);
            return this->stringContent;
      }

      // Overloading the equal (=) operator
      StrShift operator=(const string s)
      {
            *this = StrShift(s);
            return *this;
      }

private:
      string stringContent;
};

ostream &operator<<(std::ostream &os, const StrShift &v)
{
      os << v.stringContent;
      return os;
}

