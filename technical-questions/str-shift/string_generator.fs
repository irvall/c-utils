open System
let ranStr n = 
    let r = Random()
    let chars = Array.concat([[|'a' .. 'z'|];[|'A' .. 'Z'|];[|'0' .. '9'|]])
    let sz = Array.length chars in
    (Array.init n (fun _ -> chars.[r.Next sz]))

let (str, expected) = 
    ranStr 1000
    |> Array.splitAt 339
    |> fun (a,b) -> String(Array.concat (seq[b; a])), String(Array.concat (seq[a;b]))