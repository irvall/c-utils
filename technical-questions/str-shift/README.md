# Build

Run `make main` to build `main.cpp`.
Run `make test` to build and run `test.cpp` that runs some automated tests using Catch (https://github.com/catchorg/Catch2).

# Run

After building main, run following in command line:
    main (no args)
    main <string> <number of shifts>

Example:
```
$ main Microsoft 2
["Microsoft" << 2] yields "ftMicroso"
["Microsoft" >> 2] yields "crosoftMi"
```