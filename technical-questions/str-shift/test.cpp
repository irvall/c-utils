#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"
#include "shift_str.h"

TEST_CASE("Default test case") {
    StrShift str;
    str = "Microsoft";
    REQUIRE(str << 2 == "ftMicroso");
    REQUIRE(str >> 2 == "Microsoft");
}

TEST_CASE("Doing many more shifts than string length") {
    StrShift str;
    str = "Microsoft";
    REQUIRE(str << 9000 == "Microsoft");
    REQUIRE(str >> 4500 == "Microsoft");
}

TEST_CASE("Shifting zero times should do nothing") {
    StrShift str;
    str = "Microsoft";
    REQUIRE(str >> 0 == "Microsoft");
    REQUIRE(str << 0 == "Microsoft");
}

TEST_CASE("Shifting the empty string") {
    StrShift str;
    str = "";
    REQUIRE(str << 42 == "");
    REQUIRE(str >> 42 == "");
}

TEST_CASE("Shifting a longer string (1000 characters)") {
    StrShift str;
    string expected;
    str      = "aH6ainjRx7a4Yvgxwjp4BwQCwGK6e7LVn90Ckgmwh7ThoSonVEwAhE8N1DOLNnNzvpQZENmq63WENJoNsxlJKlNTamLRCLS4ttV9bzShsgDoREarpatpceZkL7YrbFXvk2luYpL2PdrERybGmuBBrD2pEKxeLAgWsheZtPDayAK8WuCn5seRdweQa50n1Hro7EmKummH1Bc033CjhjitvzS6dxZxZpi1jwbri23TfH0MtnXgDRgSy82IVqH6wZSLslRG1lapqznYIgM0JS4hjADlDDF2v6wqZotiFqpsQ0B9Ue9PNQwO4KC2SFdxPi3uc78uWEutDMBs8DBzE0oOJSvpWAAan1MH6h7WfO2jwdlsij4oeN3Z4pUtMTHweufL94v1GOuZ7frFHchMGl5K7XNRwNjwgGfc8JsoYcRzse2Y7oWJL5Z2yENmqOE3E7SA9pH7U2YEDFr34YTi0KUlRmND5f7kIY5uvBZSRYZThvy3nUrC4wc4nGTWMdbO5O5PK6HD9bAbVbewXTqlXFplVahYRcfNWkYCzTtqmYC25bjmCx82X01zyU3JZSV75WoLnL7eAa99dwpU15wudn06syeOZTADcF6jWI90A4CW12wbEjR4wA4S6ssYHsoQJF6Izg7t5F4iVdfAcckQcGhLKfxZ25XqR5xkj49jaWJXqVqJfwfZmjrSNBh79gcUxf3OBhg4GZwfEtahYYTF08dHOOHRXbk3TYAk1Mi7qltwXlotB9ilwH4ybw9WKygEPCvQ3ZqmPVoOPQUCAnqY38Hm4bjFMww1fYRuZHzLS6FKspTuLGvxisLQ5jW2QUJtywJ09zM2XOf7D63OrY3fa9TWnazQVH58wml6Q3um3CJDmOn9qiK10EszMWN9a6hpSgKqU77yDRlHAqJIWAzN7eh23Wt3pvl7vtVgd7otkmxisU8wnemJGDsN9Sw7OdiiGomhEDEZlgDUR8KiX6QliLA1ahgvKrPgeoMdI12TJxyA";
    expected = "fxZ25XqR5xkj49jaWJXqVqJfwfZmjrSNBh79gcUxf3OBhg4GZwfEtahYYTF08dHOOHRXbk3TYAk1Mi7qltwXlotB9ilwH4ybw9WKygEPCvQ3ZqmPVoOPQUCAnqY38Hm4bjFMww1fYRuZHzLS6FKspTuLGvxisLQ5jW2QUJtywJ09zM2XOf7D63OrY3fa9TWnazQVH58wml6Q3um3CJDmOn9qiK10EszMWN9a6hpSgKqU77yDRlHAqJIWAzN7eh23Wt3pvl7vtVgd7otkmxisU8wnemJGDsN9Sw7OdiiGomhEDEZlgDUR8KiX6QliLA1ahgvKrPgeoMdI12TJxyAaH6ainjRx7a4Yvgxwjp4BwQCwGK6e7LVn90Ckgmwh7ThoSonVEwAhE8N1DOLNnNzvpQZENmq63WENJoNsxlJKlNTamLRCLS4ttV9bzShsgDoREarpatpceZkL7YrbFXvk2luYpL2PdrERybGmuBBrD2pEKxeLAgWsheZtPDayAK8WuCn5seRdweQa50n1Hro7EmKummH1Bc033CjhjitvzS6dxZxZpi1jwbri23TfH0MtnXgDRgSy82IVqH6wZSLslRG1lapqznYIgM0JS4hjADlDDF2v6wqZotiFqpsQ0B9Ue9PNQwO4KC2SFdxPi3uc78uWEutDMBs8DBzE0oOJSvpWAAan1MH6h7WfO2jwdlsij4oeN3Z4pUtMTHweufL94v1GOuZ7frFHchMGl5K7XNRwNjwgGfc8JsoYcRzse2Y7oWJL5Z2yENmqOE3E7SA9pH7U2YEDFr34YTi0KUlRmND5f7kIY5uvBZSRYZThvy3nUrC4wc4nGTWMdbO5O5PK6HD9bAbVbewXTqlXFplVahYRcfNWkYCzTtqmYC25bjmCx82X01zyU3JZSV75WoLnL7eAa99dwpU15wudn06syeOZTADcF6jWI90A4CW12wbEjR4wA4S6ssYHsoQJF6Izg7t5F4iVdfAcckQcGhLK";
    REQUIRE(str << 339 == expected);
    REQUIRE(str >> 0 == expected);
}

TEST_CASE("Doing shift with negative number should be like shifting positively in the other direction") {
    StrShift str1, str2;
    str1 = "Microsoft";
    str2 = "Microsoft";
    REQUIRE(str1 << -2 == str2 >> 2);
}
