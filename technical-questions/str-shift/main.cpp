#include <iostream>
#include "shift_str.h"
int main(int argc, char const *argv[])
{
      StrShift example;
      example = argc > 2 ? argv[1] : "Microsoft";
      int shift = argc > 2 ? atoi(argv[2]) : 2;
      cout << "[\"" << example << "\" << " << shift << "] yields \"" << (example << shift) << "\"" << endl;
      example >> shift; // Undoing shift
      cout << "[\"" << example << "\" >> " << shift << "] yields \"" << (example >> shift) << "\"" << endl;
      return 0;
}
