#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"
#include "replace_str.h"

TEST_CASE("Default test case") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "ic", "MSFT");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "MMSFTrosoft"));
}

TEST_CASE("Replace a repeated pattern") {
    char *s = "iii";
    unsigned int replacements = str_replace(&s, "i", "xyz");
    REQUIRE(replacements == 3);
    REQUIRE(!strcmp(s, "xyzxyzxyz"));
}

TEST_CASE("Replace with empty pattern does nothing") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "", "MSFT");
    REQUIRE(replacements == 0);
    REQUIRE(!strcmp(s, "Microsoft"));
}

TEST_CASE("Replace with empty replacement string removes that part") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "Micro", "");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "soft"));
}

TEST_CASE("Find entire string, replace with itself") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "Microsoft", "Microsoft");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "Microsoft"));
}

TEST_CASE("Replace first letter") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "M", "N");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "Nicrosoft"));
}

TEST_CASE("Replace last letter") {
    char *s = "Microsoft";
    unsigned int replacements = str_replace(&s, "t", "tice");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "Microsoftice"));
}

TEST_CASE("Replace something in the middle") {
    char *s = "abc";
    unsigned int replacements = str_replace(&s, "b", "hello world");
    REQUIRE(replacements == 1);
    REQUIRE(!strcmp(s, "ahello worldc"));  
}

TEST_CASE("Replace repeating pattern with almost identical chunk") {
    char *s = "iiiii";
    unsigned int replacements = str_replace(&s, "ii", "xyz");
    REQUIRE(replacements == 2);
    REQUIRE(!strcmp(s, "xyzxyzi"));  
}