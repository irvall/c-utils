#include <stdio.h>
#include <stdlib.h>
#include <string.h> // Only for string length

// Replaces string in-place - needs a pointer to the string for that
// reason. General idea: build up a new string by traversing input string, if
// we find a chunck identical to our pattern (resulting in p == plen), inject
// replacement string, otherwise, we add original char from input.
unsigned int str_replace(char **s, char *pattern, char *replace) 
{
    char *res;
    size_t i, resi;
    size_t slen, plen, rlen; 
    unsigned int count;
    slen = strlen(*s);
    plen = strlen(pattern);
    rlen = strlen(replace);
    if(plen == 0) 
        return 0;
    i = resi = count = 0;
    res = (char *) malloc(1 + slen*rlen);
    while(i < slen) {
        size_t p = 0;
        for(int j = i; p < plen; p++, j++) 
            if(pattern[p] != (*s)[j]) break;

        if(p == plen) {
            count++;
            i += plen;
            for(size_t r = 0; r < rlen; r++) 
                res[resi++] = replace[r];
        } else res[resi++] = (*s)[i++];

    }
    res[resi] = '\0';
    *s = res;
    return count;
}