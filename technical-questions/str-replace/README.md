# Build

Run `make main` to build `main.c`.
Run `make test` to build and run `test.cpp` that runs some automated tests using Catch (https://github.com/catchorg/Catch2).

# Run

After building main, run following in command line:
    main (no args)
    main <string> <pattern> <replacement>

Example:
```
$ main Hello ello 2O
Did 1 replacement -> "H2O"
```