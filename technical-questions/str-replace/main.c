#include "replace_str.h"
int main(int argc, char *argv[]) {
    char *s = argc > 3 ? argv[1] : "Microsoft";
    unsigned int r = str_replace(&s, argc > 3 ? argv[2] : "ic", argc > 3 ? argv[3] : "MSFT");
    printf("Did %u replacement%s -> \"%s\"\n", r, r > 1 || r == 0 ? "s" : "", s);
    return 0;
}